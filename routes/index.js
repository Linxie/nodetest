const express = require('express');
const router = express.Router();
const {ensureAuthenticated} = require('../config/auth');

const Task = require('../models/Tasks');

router.get('/', (req, res) => res.render('welcome'));

router.get('/dashboard', ensureAuthenticated,(req, res) => res.render('dashboard', {
    name: req.user.name
}));
router.get('/dashboard/tasks', ensureAuthenticated,(req, res) => res.render('dashboardusers', {
    name: req.user.name
}));
router.get('/dashboard/tasks/addtask', ensureAuthenticated,(req, res) => res.render('addtask', {
    name: req.user.name
}));

router.post('/dashboard/tasks/addtask', (req, res) => {
    const { name, description } = req.body;
    
    
    Task.findOne({ name: name})
        .then(user => {
            const newTask = new Task({
                name,
                description
            });
                        
            newTask.save()
                .then(user =>{
                    req. flash('success_msg', 'Task registered');
                    res.redirect('/dashboard/tasks');
                })
                .catch(err => console.log(err));

                
            
        });

});

router.get('/get-data', (req, res) => {
    Task.find({}, (err, doc) =>{
        if(err){ throw err;}
        else{
            res.json(doc);
        }

    });
});
           


module.exports = router;